Namespaced Event Processing
===

Пространство имен образовано вложенными друг в друга множествами типов событий:

```js
import signals.paid-signals-from-vasia-pupkin from "mypaidshit/paid-signals-from-vasia-pupkin"

namespace signals {

    // Trend-related events
    // Emitted by a trend-detecting module
    namespace ema-trend-detecting:trend {
        event changed; // Fired on any change

        event confirmed; // Fired when trend has been stabilized
        event uncertain; // Fired when trend detection is uncertain

        event up;      // Fired on up-trend
        event down;    // Fired on down-trend
        event flat;    // Fired when detected no trend

    }


    // Trend-related events
    // From your paid subscription
    namespace paid-signals-from-vasia-pupkin {

        // Vasia's Signals is a 3-rd party service and as such
        // may go f***cking offline and you better know that to
        // fall back to another strategy, for example.
        namespace status {
            event online;  // Vasia is online and posting shit to his channel
            event offline; // Vasia is offline and posting shit to his channel
            event unpaid;  // You need to pay Vasia to use service
            event paid;    // You have paid Vasia
        }

        // Trend-related events. Vasia may flip coin to get that data, so
        // You'd better trust Vasia or not using his shit at all.
        namespace trend {
            event changed; // Fires on any change of Vasia's consideration

            event confirmed; // Fires when Vasia believes he has been correct
            event uncertain; // Fires when Vasia no longer believes so

            event up;      // Fires when Vasia thinks it's going UP
            event down;    // Fires when Vasia thinks it's going DOWN
            event flat;    // Fires when Vasia thinks it's FLAT
        }

        // Mirroring trading events from Vasia Pupkin's account
        namespace trading {
            event buy;
            event sell;
            event cancel;
            ...
        }

    }
}
```

Примерами событий в данном случае могут быть:

```typescript=
event signals:ema-trend-detecting:trend:up = {{
  emitter  : DetectionPlugin,
  time     : Timestamp,
  stream   : StreamId,
  security : SecurityId,
  previous : DIRECTIONS.*;
  direction: DIRECTIONS.UP;
}};
```

Каждое конкретное событие является объектом с некоторым набором полей, в зависимости от типа события.

Еще примеры:

```typescript=
event signals:ema-trend-detecting:trend.down    = {{ emitter, time, stream, security, previous, direction = DIRECTIONS.DOWN }};
event signals:ema-trend-detecting:trend:flat    = {{ emitter, time, stream, security, previous, direction = DIRECTIONS.FLAT }};
event signals:ema-trend-detecting:trend:changed = {{ emitter, time, stream, security, previous, direction }};
```

Где определен перечисляемый тип `DIRECTIONS`:

```typescript=
enum DIRECTIONS {
    UNCERTAIN = 0 << 0,
    CONFIRMED = 1 << 0,
    FLAT      = 0 << 2,
    TREND     = 1 << 2,
    UP        = 0 << 1,
    DOWN      = 1 << 1
}

//                        IS_TREND?   DIRECTION   CERTAIN?
// CONFIRMED UP   TREND = (1 << 2) || (0 << 1) || (1 << 0) = b101 = 5
// CONFIRMED DOWN TREND = (1 << 2) || (1 << 1) || (1 << 0) = b111 = 7
// UNCERTAIN DOWN TREND = (1 << 2) || (1 << 1) || (0 << 0) = b110 = 6
//
```

Если требуется подписаться на все события определенного типа, вне зависимости от того, каким плагином были получены данные, нужно использовать символ "*".
Он соответствует любому содержимому сегмента. Например:

```txt
KeyPath "signals:*:trend.down" допускает события:
    - "signals:ema-trend-detecting:trend.down"
    - "signals:paid-signals-from-vasia-pupkin:trend.down"
```