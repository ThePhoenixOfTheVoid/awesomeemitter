import { HandlerDefinition } from "./EventInterfaces";
import { buildPattern } from "./Namespaced";
import { AwesomeEmitter } from "./AwesomeEmitter";
import debug from "debug";

const $log_low = debug("low:events");
const $log = debug("log:events");

export interface EventHandler<A,T> extends HandlerDefinition<A,T> {
	test(actualPath:string): boolean;
	fire(actualPath:string, argument:A, query: Array<EventHandler<any, any>>)
}

class ThisContext<A, T> {

	constructor (
		private ehandler: EventHandler<A, T>, 
		public path: string,
		private next: Array<EventHandler<any, any>>
	) {}

	
	get context() {
		return this.ehandler.context
	}

	get priority() {
		return this.ehandler.priority
	}

	get pattern() {
		return this.ehandler.keypath
	}

	public stopPropagation() {
		this.next.splice(0, this.next.length);
	}
}




export class EventHandler<A,T> {

	private _regexp: RegExp;

	private destroyed: boolean

	constructor (public emitter: AwesomeEmitter, definition: HandlerDefinition<A,T>) {
		const kpath = definition.keypath;

		$log("created", definition.once ? "(once handler)" : "(handler)",  definition.keypath, definition.priority)

		// Doing so makes HandlerDefinition's properties immutable
		Object.defineProperties(this, {
			keypath  : { value: definition.keypath , enumerable: true  },
			priority : { value: definition.priority, enumerable: true  },
			once     : { value: definition.once    , enumerable: true  },
			handler  : { value: definition.handler , enumerable: false },
			context  : { value: definition.context , enumerable: false },
			_regexp  : { value: buildPattern(kpath), enumerable: false },
			emitter  : { value: emitter            , enumerable: false }
		})
		
		Object.defineProperty(this, "destroyed", { value: false, writable: true })

		// Adding this event handler to an event emitter
		this.emitter._callbacks.add(this);
	}

	public test(actualPath:string): boolean {
		return this._regexp.test(actualPath)
	}

	public fire(actualPath:string, argument:A, query: Array<EventHandler<any, any>>) {
		if (this.destroyed || !this.test(actualPath)) return false
		$log_low("Processing event", {
			ap: actualPath,
			kp: this.keypath, 
			pr: this.priority
		})
		this._fire.call(this, actualPath, argument, query)
		return true
	}

	private _fire(path:string, argument:A, next: Array<EventHandler<any, any>>) {
		if (this.destroyed) return false
		const thisCtx = new ThisContext(this, path, next);
		this.handler.call(thisCtx, path, argument, this.context, this);
		if (this.once) this._destroy()
	}

	private _destroy() {
		this.emitter._callbacks.delete(this);
		$log("destroy", this.once ? "(once handler)" : "(handler)",  this.keypath, this.priority)

		// @final
		Object.defineProperty(this, "destroyed", { value: true, writable: false })
	}

}

