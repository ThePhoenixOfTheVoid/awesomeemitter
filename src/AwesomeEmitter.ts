import assert from "assert";

import { IEEmitter, EventHandlerFunction } from "./EventInterfaces";
import { EventHandler } from "./EventHandler";


function resolvePriorityAndContext(...A) {
	const priority = (typeof A[0] === "number") ? A.shift() : 100;
	const context  = (typeof A[0] === "object") ? A.shift() : null;
	assert(A.length == 0, "incorrect arguments signature");
	return { priority, context };
}


function resolveKeypathHandlerAndContext(...A) {
	const keypath = (typeof A[0] === "string")   ? A.shift() : undefined;
	const handler = (typeof A[0] === "function") ? A.shift() : undefined;
	const context = (typeof A[0] === "object")   ? A.shift() : undefined;
	assert(A.length == 0, "incorrect arguments passed to Emitter#off")
	return { keypath, handler, context }
}


export class AwesomeEmitter implements IEEmitter {

	_callbacks = new Set();

	/** 
	 * @param keypath namespaced event name (segments separated by ":")
	 * @param handler handler for specified event
	 * @param context a context that will be passed to the handler
	 * @returns itself so calls can be chained
	 */
	public addListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>, ...rest){
		const { priority, context } = resolvePriorityAndContext(...rest);
		return new EventHandler<A,T>(this, { keypath, handler, context, priority, once: false })
	}

	/**
	 * @param keypath namespaced event name (segments separated by ":")
	 * @param handler handler for specified event
	 * @param context a context that will be passed to the handler
	 * @returns itself so calls can be chained
	 */
	public addOnceListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>, ...rest) {
		const { priority, context } = resolvePriorityAndContext(...rest);
		return new EventHandler<A,T>(this, { keypath, handler, context, priority, once: true  })
	}


	/** 
	 * @param keypath (if provided) matching this keypath
	 * @param handler (if provided) holding this handler reference 
	 * @param context (if provided) holding this context reference 
	 * @note if no arguments are provided, ALL handlers will be removed.
	 * @returns itself so calls can be chained
	 */
	public removeListener(...rest){
		const { keypath, handler, context } = resolveKeypathHandlerAndContext(...rest)

		this._callbacks.forEach(item => {
			if (keypath && item.keypath !== keypath) return
			if (handler && item.handler !== handler) return
			if (context && item.context !== context) return
			item._destroy()
		})

		return this;
	}

	/**
	 * @param keypath event's keypath
	 * @param arg     event's payload
	 */
	public emit<A>(keypath: string, arg?: A) {
		const current = [];
		this._callbacks.forEach(item => {
			if (item.test(keypath)) current.push(item);
		})
		
		current.sort((a, b) => b.priority - a.priority)

		while (current.length) {
			const item = current.shift();
			item.fire(keypath, arg, current)
		}
	}

	/**
	 * @alias removeListener
	 * @see {@link EEmitter.removeEventListener}
	 */
	public off(...rest){
		return this.removeListener.apply(this, arguments)
	}

	/**
	 * @method on
	 * @alias addListener
	 */

	public on<A,T>(keypath:string, handler: EventHandlerFunction<A,T>, ...rest){
		return this.addListener.apply(this, arguments)
	}

    /**
	 * @method once
	 * @alias addOnceListener
	 */
	public once<A,T>(keypath:string, handler: EventHandlerFunction<A,T>, ...rest){
		return this.addOnceListener.apply(this, arguments)
	}

}


/**
 * @example Using event emitter: namespacing and stopPropagation feature.
 * 
 * ```typescript
 * const emitter = new EEmitter();
 * const context1 = { some: "context1" }
 * const context2 = { some: "context2" }
 * const context3 = { some: "context3" }
 * 
 * function showThisCtx () {
 *  console.log(this);
 * }
 * 
 * function stopPropagation () {
 *  this.stopPropagation()
 *  console.log(this);
 * }
 * 
 * emitter.once("event:new:hello:*", stopPropagation, 200, context1)
 * emitter.once("event:new:hello:*", showThisCtx, context2)
 * emitter.on("event:new:hello:#",   showThisCtx, context3)
 * 
 * 
 * 
 * console.log(emitter)
 * 
 * emitter.emit("event:new:hello:fuck", "shit") // 3 times
 * emitter.emit("event:new:hello:fuck", "shit") // 2 times
 * emitter.emit("event:new:hello:fuck:only1", "shit") // 1 time
 * ```
 */

 /*
const emitter = new AwesomeEmitter();
const context1 = { some: "context1" }
const context2 = { some: "context2" }
const context3 = { some: "context3" }

function showThisCtx () {
 console.log(this);
}

function stopPropagation () {
 this.stopPropagation()
 console.log(this);
}

emitter.once("event:new:hello:*", stopPropagation, 200, context1)
emitter.once("event:new:hello:*", showThisCtx, context2)
emitter.on("event:new:hello:#",   showThisCtx, context3)



console.log(emitter)

emitter.emit("event:new:hello:fuck", "shit") // 3 times
emitter.emit("event:new:hello:fuck", "shit") // 2 times
emitter.emit("event:new:hello:fuck:only1", "shit") // 1 time
*/