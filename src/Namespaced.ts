import assert from "assert";
import debug from "debug";

const $log_compiling = debug("log:events:compile")
const $log_reusing   = debug("low:events:reusing")
const $log_error     = debug("error:events")


function setIn(object, path: string[], value) {
	path.reduce((deep, seg, i) => {
		if (path.length - 1 === i) return deep[seg] = value
		return deep[seg] = deep[seg] || Object.create(null);
	}, object)
	return object
}

function getIn(object, path: string[]) {
	for (var i = 0, len = path.length; i < len; ++i) {
		if (object == null) return 
		object = object[path[i]];
	}
	return object;
}

export const SingleSegmentRegExp = /^[a-z0-9-]+$/;



/**
 * NamespacedEventToolkit
 * @module events/namespaced
 * 
 * Contains functions to work with namespaced events. 
 * 
 * Name segments are separated by ":". 
 * 
 * In patterns each segment is either:
 *   1. Valid name satisfying RegExp /[a-z0-9-]+/
 *   2. "*" (wildcard) that matches exactly one segment.
 *   3. "#" (trailing wildcard) that matches any (including zero) segments at the end
 */

const __RegExpCache__ = {}

/**
 * @function validatePatternSegment
 * 
 * Validates that given pattern segment is legal. 
 * Function signature matches the Array#every required signature.
 * 
 * @param segment  current path segment
 * @param position index of current path segment
 * @param segments array of all path segments
 * 
 * @returns if this path segment is valid
 */

export function validatePatternSegment(segment: string, position: number, segments: string[]): boolean {
	if (segment === "*") return true;
	if (segment === "#") return position === segments.length - 1;
	return SingleSegmentRegExp.test(segment)
}

/**
 * @function validateNamePatternSegment
 * 
 * Validates that given name segment is legal. 
 * Function signature matches the Array#every required signature.
 * 
 * @param segment  current path segment
 * @param position index of current path segment
 * @param segments array of all path segments
 * 
 * @returns if this name part is valid
 */
export function validateKeypathSegment(segment: string, position: number, segments: string[]): boolean {
	return SingleSegmentRegExp.test(segment)
}

/**
 * Creates a compiled RegExp object from a given string pattern. 
 * Validates that input is correct and returns plain RegExp.
 */
export function buildPattern(pattern: string) {
	const segments = pattern.split(":");
	let result = getIn(__RegExpCache__, segments);

	if (result) {
		$log_reusing(pattern);
		return result;
	}

	$log_compiling(pattern);
	try {
		result = _buildPattern(segments);
	} catch (error) {
		$log_error(error);
		throw error;
	}
	setIn(__RegExpCache__, segments, result);

	return result
}



function _buildPattern(segments: string[]) {
	assert(segments.length > 0, "cannot be empty string");
	assert(segments.every(validatePatternSegment), "cannot be empty string");
	const reSrc = "^" + segments.map(part => {
		if (part === "*") return "[a-z0-9-]+";
		if (part === "#") return "[a-z0-9-:]*";
		return part
	}).join("[:]") + "$"
	return RegExp(reSrc)
}
