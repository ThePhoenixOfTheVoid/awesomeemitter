import { EventHandler } from "./EventHandler";

/**
 * Wrapper type for context (of type T).
 */
export type Context<T> = T;

/**
 * @type EventHandler<A,T>
 * 
 * Wrapper type for handler that expects 
 * an argument of type A and context of type T.
 * 
 * @param name      namespaced event name (segments separated by ":")
 * @param argument  any payload for current event
 * @param context   context for current event
 */
export type EventHandlerFunction<A,T> = (name:string, argument:A, context?:Context<T>, handler?: HandlerDefinition<A, T>) => void;


/** A plain object holding hanlder definition for a specific event. */
export interface HandlerDefinition<A,T> {
	/** namespaced event name pattern */ 
	keypath: string;
	/** positive number defining the order of execution */
	priority: number;
	/** function that will be invoked to handle event */
	handler: EventHandlerFunction<A,T>;
	/** should the handler be invoked at most once */
	once: boolean;
	/** context event that will be passed to handler */
	context: Context<T>;
}


export interface IEEmitter {
	/**
	 * Adds a new handler for an specified event.
	 */
	addListener<A>  (keypath:string, handler: EventHandlerFunction<A,null>, priority: number /** null context */ ): EventHandler<A,null>;
	addListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    /**std priority*/ context: Context<T>): EventHandler<A,T>;
	addListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    priority: number, context: Context<T>): EventHandler<A,T>;

	/**
	 * Adds a new handler for specified event (or group of events).
	 * This handler will be invoked at most once.
	 */
	addOnceListener<A>  (keypath:string, handler: EventHandlerFunction<A,null>, priority: number /** null context */ ): EventHandler<A,null>;
	addOnceListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    /**std priority*/ context: Context<T>): EventHandler<A,T>;
	addOnceListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    priority: number, context: Context<T>): EventHandler<A,T>;

	/**
	 * Removes events handlers
	 */
	removeListener<A>(): this;
	removeListener<A>(keypath:string): this;
	removeListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>): this;
	removeListener<A,T>(keypath:string, context: Context<T>): this;
	removeListener<A,T>(keypath:string, handler: EventHandlerFunction<A,T>, context: Context<T>): this;

	/**
	 * Removes events handlers
	 */
	off<A>(): this;
	off<A>(keypath:string): this;
	off<A,T>(keypath:string, handler: EventHandlerFunction<A,T>): this;
	off<A,T>(keypath:string, context: Context<T>): this;
	off<A,T>(keypath:string, handler: EventHandlerFunction<A,T>, context: Context<T>): this;

	/**
	 * Adds a new handler for an specified event.
	 */
	on<A>  (keypath:string, handler: EventHandlerFunction<A,null>, priority: number /** null context */ ): EventHandler<A,null>;
	on<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    /**std priority*/ context: Context<T>): EventHandler<A,T>;
	on<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    priority: number, context: Context<T>): EventHandler<A,T>;

	/**
	 * Adds a new handler for specified event (or group of events).
	 * This handler will be invoked at most once.
	 */
	once<A>  (keypath:string, handler: EventHandlerFunction<A,null>, priority: number /** null context */ ): EventHandler<A,null>;
	once<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    /**std priority*/ context: Context<T>): EventHandler<A,T>;
	once<A,T>(keypath:string, handler: EventHandlerFunction<A,T>,    priority: number, context: Context<T>): EventHandler<A,T>;

	/**
	 * Fires an event with specified keypath and payload.
	 */
	emit<A>(keypath: string, arg: A);
}
