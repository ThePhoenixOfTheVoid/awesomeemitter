# AwesomeEmitter

An event-emitter with namespace, priority, handler context, wildcards (* and #) and event intercepting support, written in TypeScript for use in Node.JS and Browser, for projects where reliability is a must. Focused on synchronous event processing, but can be used to handle asynchronous ones too.